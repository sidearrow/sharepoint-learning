# PowerAppsの利用
## 1. PowerApps概要
- PowerAppsはクロスプラットフォーム対応のアプリをノンコーディングで作成できる
- SharePoint上のデータを利用できる
## 2. PowerAppsの利用環境
- 開発ツールは2種類提供されている
  - Windowsデスクトップアプリ  
    `PowerApps Studio for Windows`
  - Webアプリ  
    `PowerApps Studio for Web`
## 3. モダンリストでのアプリ作成 - 基本
- GUIで画面作成できる
- 保存後、バージョン発行を行うと利用できる
## 4. モダンリストでのアプリ作成 - Flowと連携
- この節でFlowとの連携について記載されていない
## 5. SharePointをデータソースとしたアプリ作成
- MicrosoftApps開発ツール上でSharePointサイトURLを指定しリストを選択すると、データソースが追加される
## 6. PowerAppsアプリの共有と管理
- バージョン管理可能
  - 過去のバージョンを簡単に復元できる