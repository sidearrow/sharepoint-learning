# SharePointFrameworkによるクライアントサイドWebパーツ開発
## 1. SharePointFramework概要
- モダンページ向け
## 2. 利用するツールと開発環境の準備
- TypeScript
  - Microsoft開発の静的型付け機能を持つ言語
  - コンパイルするとJavaScriptが生成される
- Node.js
  - サーバーサイドJS環境
- npm
  - パッケージ管理ツール
- Gulp
  - タスクランナー
- YEOMAN
  - プロジェクト作成支援ツール
  - `SharePoint Yeoman Generater`が用意されている
  - npmからグローバルでインストール
    ```
    npm install -g yo
    ```
- ソースコードエディタ
  - TypeScriptデバッグができれば便利
  - VSCodeを使用する
## 3. クライアントサイドWebパーツの開発 - 基本
- 公式のSharePointプロジェクトジェネレータ
```bash
$ yo @microsoft/sharepoint
```
- JSフレームワークを使用するか、使用する場合は`React`or`Knockout.js`を選択できる
  - Reactを選択
- 完了画面が表示される
  ![](./img/8_1.jpg)