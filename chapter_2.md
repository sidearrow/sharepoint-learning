# SharePointサイトデザインのしくみとカスタマイズ
## 1. SharePointページモデル
### 1-1. マスターページ
- ASP.NE技術のマスターページ
- マスターページ（.master）+ コンテンツページ（.aspx）= 実際の画面
- 標準で用意されている
### 1-2. マスターページに含まれる内容
```html
<%@Master language="C#"%>
<%@ Register %>
<%@ Import %>
<!DOCTYPE html>
<asp: >
<SharePoint: />
```
### 1-3. マスターページより参照しているCSSファイル
- `\TEMPLATE\LAYOUTS\1041\STYLES`に格納されている
- カスタマイズは別途CSSファイルを作成
### 1-4. マスターページ内のコンテンツプレースホルダー
- マスターページとコンテンツページは`ContentPlaceHolder`を利用して連動  
- 主なContentPlaceHolderコントロール
  #### 
  |||
  |--|--|
  | PlaceHolderTopNavBar | グローバルナビゲーション |
  | PlaceHolderSearchArea | 検索ボックス |
  | PlaceHolderPageTitleInTitleArea | サイトタイトル, ページタイトル |
  | PlaceHolderLeftNavBar | 左カラムリンクバー |
  | PlaceHolderMain | メインカラム |
- id属性を使用して連携
  ```html
  // マスターページ
  <asp:ContentPlaceHolder id="PlaceHolderMain" runat="server" />

  // コンテンツページ
  <asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    // ページ内容
  </asp:Content>
  ```
- ダウンロード最小化戦略  
  ページ遷移時に必要部部のみレンダリングする機能  
  SinglePageApplication
### 1-5. 発行ページとページレイアウト
- テンプレートを利用してページを複製できる
## 2. サイトデザインカスタマイズ方法
### 2-1. 設定ベースで行えるデザイン変更
- ロゴや外観をカスタマイズ不要で変更できる
### 2-2. サイトの全体デザインに対するカスタマイズ
### 2-3. 特定のコンテンツ表示に対するカスタマイズ
## 3. デザインカスタマイズ時に考慮すべきこと
- ポータルサイトとチームサイトで求められる要件の違い
- 実装量、軽量
- マスターページは製品アップデートの影響を受けるため、出来る限りカスタマイズは避ける
## 4. 代替CSSによるカスタマイズ
## 5. ユーザーカスタムアクションを利用したJavaScript組み込み
- JSファイルの保存場所
  - サイトコレクション全体に適用する場合......スタイルライブラリ
  - サイトに対して適用する場合......サイトのリソースファイル  

以降のページは適宜必要になった際に参照する
