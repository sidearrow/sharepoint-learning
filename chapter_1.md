# 1. SharePointカスタマイズ/開発の概要
## 1-1. 概要
- 様々なニーズに対応できる情報プラットフォーム
- 標準提供されている機能はブラウザベースの設定画面から利用できる
- より詳細な業務ニーズに対応する仕組みもある
  - カスタマイズ
    - 別途ツールやサービスを利用する
    - HTML, CSS, JSでカスタマイズ
  - 開発
    - パッケージで配布
- クラシックサイトとモダンサイト

## 1-2. SharePoint API
- クライアントAPIとサーバーAPI
### 1-2-1. クライアントAPI
- SharePointサーバー外で実行される
- RESTとクライアントサイドオブジェクトモデルの二種類
#### REST API
- HTTPリクエストで利用
- GET以外はリクエストヘッダに`X-RequestDigest`設定が必要  
  値は`$("#__REQUESTDIGEST").val()`で取得できる  
  jQueryが使える
```JavaScript
// POSTリクエスト例

$.ajax({
  url: "URL",
  method: "POST",
  data: "",
  headers: {
    "accept": "application/json;odata=verbose",
    "content-type": "application/json;odata=verbose",
    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
  },
  success: successHandler,
  error: errorHandler,
});
```
#### クライアントサイドオブジェクトモデル
- JS
  - `new SP.ClientContext()`で利用できる
- .NET
  - 再頒布可能パッケージを利用
### 1-2-2. サーバーAPI
- SharePointサーバー上での実行が必要  
→ SharePointOnlineでは利用不可  
- 推奨ではない
## 1-3. カスタマイズ方法
- PowerApps, MicrosoftFlowと連携
- Web標準技術を利用したカスタマイズ
  - モダンサイトはカスタムスクリプトが実行できないため、SharePointFrameworkクライアントサイドWebパーツを利用する
## 1-4. パッケージとして展開が行える開発方法
### 1-4-1. SharePointアドイン
- 別環境で動作するWebアプリを開発し、クライアントAPIを使用し連動させる
- 開発されたWebパーツは『アドインパーツ』と呼ばれる
### 1-4-2. SharePointFramework
- 一番新しい開発モデル
- JSのみ  
  → クライアントAPIはJSOM or REST
- 開発されたWebパーツは『クライアントサイドWebパーツ』と呼ばれる
### 1-4-3. ファームソリューション
- 非推奨
### 1-4-4. 開発方法選択ポイント
|  | SharePointアドイン | SharePointFramework |
|--|--|--|
| 実行環境 | クライアントサイド/サーバーサイド | クライアントサイドのみ |
| API |  | JSOM, REST |
| 動作 | iframe | 直接 |
| 対象サイト | クラシック | モダン |
| 開発環境 | VisualStudio | OSS |
